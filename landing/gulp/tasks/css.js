var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sync = require('browser-sync'),
    util = require('../util'),
    uncss = require('gulp-uncss');

gulp.task('css', function () {  

    gulp.src('./scss/**/*.scss')
        .pipe(sass({
            includePaths: ['scss']
        }))

        // Report compile erros
        .on('error', util.handleErros)

        // Specify the output destination
        .pipe(gulp.dest('./dist/css'))

        // Reload the browser-sync
        .pipe(sync.reload({stream:true}));

    gulp.src('./dist/css/main.css')
        .pipe(uncss({
            html: ['./index.html']
        }))
        .pipe(gulp.dest('./dist/css'));

});


