var gulp = require('gulp');

gulp.task('default', ['html', 'sync', 'css', 'img'], function () {
    gulp.watch('./js/*.js', ['js']);
    gulp.watch('./scss/*.scss', ['css']);
    gulp.watch('./*.html', ['html']);
});

gulp.task('img', function () {
    gulp.src('./img/*')
    .pipe(gulp.dest('./dist/img/'));
});

gulp.task('html', function () {
    gulp.src('./index.html')
    .pipe(gulp.dest('./dist/'));
});
