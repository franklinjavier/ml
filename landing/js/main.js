(function() {

    'use strict';

    var regexEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
        mail = document.querySelector('#email');

    document.querySelector('#send').onclick = function() {
        if ( regexEmail.test(mail.value) ) {
            alert('Gracias!');
            location.reload();
        } else {
            alert('E-mail Incorrecto.');
            mail.focus();
        }
    };
    
    document.querySelector('#sendFooter').onclick = function() {
        mail.focus();
    };

}());
