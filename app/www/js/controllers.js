function format(num, decimals, dec_point, thous_sep) {
  num = num.toString().split('.');
  num[0] = num[0].split('').reverse().join('').replace(/(\d{3})(?=\d)/g, '$1'+ (thous_sep || '.')).split('').reverse().join('');
  if (decimals == undefined) {
    decimals = 2;
  }
  if (decimals) {
    num[1] = num[1] ? num[1].slice(0, decimals) : '';
    if (num[1].length < decimals)
      num[1] += new Array(decimals - num[1].length + 1).join('0');
  }
  else if (num[1]) {
    num.pop();
  }
  return num.join(dec_point || ',');
}

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {


})

.controller('PaymentCtrl', function($scope, $rootScope, $stateParams) {

  var $value = $('#value'),
    tax = 13.00;

  $scope.value = $rootScope.value || '0,00';
  $scope.tax = format(13.00, 2);

  $('.numpad').on('click', function(e) {
    e.preventDefault();

    var digit = $(this).text();

    // remove digit
    if (digit === '') {
      $scope.value = $scope.value.slice(0, -1);
    } else {
    // add digit
      $scope.value += digit;
    }

    $scope.value = $scope.value.trim();
    $scope.$apply();
    $value.setMask().keyup();

    $rootScope.value = $value.val();
    var total = +$value.val().replace('.', '').replace(/,/, '.');

    $rootScope.portion = format((total + tax)/ 3, 2);

  });

  $scope.reset = function() {
    window.location.href = '#/app/payment';
    window.location.reload();
  };

  setTimeout(function() {
    $value.setMask().keyup();
  }, 1);

});
